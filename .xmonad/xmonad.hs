import XMonad
import qualified Data.Map as M
import Graphics.X11.ExtraTypes.XF86

myKeys x =
  [ ( ( modMask x .|. shiftMask, xK_l ), spawn "slock" )
  , ( ( 0, xF86XK_AudioMute ), spawn "amixer set Master 0%" )
  , ( ( 0, xF86XK_AudioLowerVolume ), spawn "amixer set Master 10%-" )
  , ( ( 0, xF86XK_AudioRaiseVolume ), spawn "amixer set Master 10%+" )
  ]

newKeys x = M.union ( keys defaultConfig x ) ( M.fromList ( myKeys x ) )

main = do
  xmonad $ defaultConfig
    { terminal           = "termite -e tmux"
    , keys               = newKeys
    , normalBorderColor  = "#cccccc"
    , focusedBorderColor = "#ccffcc"
    }
