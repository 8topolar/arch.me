# Arch Linux

## configure

* xmonad
* termite
* vim

## install

`curl -kfsSL https://gitlab.com/8topolar/arch.me/raw/master/install.sh | bash`

## if xmonad

`xmonad --recompile`
