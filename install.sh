#!/usr/bin/env bash

# clone
git clone --recursive https://gitlab.com/8topolar/arch.me.git ~/.me

# save work directory
pushd .

# install
cd ~/.me && ./.install

# return to work directory
popd

echo "thanks!"
